from pydantic.dataclasses import dataclass
import os
from enum import Enum
from typing import List

from flask import Flask
from flask_pymongo import PyMongo
from flask_login import LoginManager
from pymongo import ASCENDING, DESCENDING
from flask_jwt import JWT, jwt_required, current_identity

from flask_mongo_orm.entity_registry import EntityRegistry
from flask_mongo_orm.entity import Entity, PydanticEntity, TimestampedMainModel, EntityBaseModel
from flask_mongo_orm.field import EnumField

from flask_mongo_orm.field import Field

registry = EntityRegistry()
login_manager = LoginManager()


def create_app(config_filename=None):
    mongo = PyMongo()
    app = Flask(__name__, instance_relative_config=True,
                static_url_path='/static')
    app.config.from_pyfile(config_filename)
    app.config['MONGO_URI'] = "mongodb://{}/{}".format(
        'localhost', 'db'
    )
    mongo.init_app(app)
    login_manager.init_app(app)
    login_manager.login_view = 'file-share.login'
    registry.init_app(app, mongo)
    return app


class Gender(Enum):
    MALE = 0
    FEMALE = 1
    OTHER = 2


@registry.restgister()
@registry.register()
class User(Entity):

    collection_name = 'users'

    typed_fields = {
        'roles': Field(pytype=list),
        'name': Field(pytype=str),
        'username': Field(pytype=str),
        'gender': EnumField(Gender)
    }

    required_fields = typed_fields

@registry.restgister(
)
@registry.register(
)
@registry.register_collection_bootstrap()
class Player(Entity):

    collection_name = 'players'

    typed_fields = {
        'name': Field(pytype=str),
        'username': Field(pytype=str)
    }

    indexes = Entity.indexes + (
        {'fields': [('name', ASCENDING), ('username', ASCENDING)],
         'options': {'unique': True}},
        #{'fields':[('name', ASCENDING)]}
    )

    required_fields = typed_fields


class Leaner(EntityBaseModel):

    name: str = 'lean'


class PlayerModel(TimestampedMainModel):
    name: str
    leaners: List[Leaner] = []
    genender: Gender = Gender.MALE.value
    als: List[str] = []
    password: str = 'helloisitmeyouarelookingfor'
    roles: List[str] = ['USER']


@registry.restgister(
    methods={'C': ('ADMIN',), 'R': ('ADMIN',),
             'U': ('USER',), 'D': ('ADMIN',)}
)
class PlayerEntity(PydanticEntity):

    collection_name = 'players222'
    _model_class = PlayerModel


@login_manager.user_loader
def load_user(user_id):
    return User(name='bobby', username='lean', roles=['ADMIN'])


def authenticate(username, password):
    user = PlayerEntity.find_one({username: 'name'})
    # TODO: passwd validation
    return user


def identity(payload):
    user_id = payload['identity']
    return PlayerEntity.find_one(user_id)


app = create_app(config_filename='dev.py')

jwt = JWT(app, authenticate, identity)


@app.route('/test', methods=['GET'])
def test12():
    leaner = Leaner(name='name')
    player = PlayerEntity(name='name', leaners=[leaner, {'name': 'test'}])
    player.save()
    player.schema()
    return {'entries': [e.json_format() for e in PlayerEntity.find()]}


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9000)
