from enum import Enum

from flask_mongo_orm.field import EnumField


class Status(Enum):
    DONE = 0
    STARTED = 1


field = EnumField(Status)

choices = tuple(Status)

for f in field:
    print(f)

for c in Status:
    print(c)

for name, member in Status.__members__.items():
    print(name, member)

for name in tuple(Status.__members__.items()):
    print(name)