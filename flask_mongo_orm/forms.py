from flask_wtf import FlaskForm
from wtforms import StringField, FieldList, SelectField
from .field import EnumField
from .exceptions import ValidationException, FieldValidationException

class FlaskEntityForm2(FlaskForm):
    '''
    Enhances flask form with a save method which saves the corresponding
    entity on the form.
    '''
    def __init__(self, entityCls, *args, instance=None, **kwargs):
        super().__init__(*args, obj=instance, **kwargs)
        self.entityCls = entityCls
        self.instance = instance

    def save(self, **kwargs):
        if not self.instance:
            self.instance = self.entityCls()
        try:
            complete_data = dict(self.data)
            complete_data.update(kwargs)
            return self.instance.save(**complete_data)
        except ValidationException as e:
            if isinstance(e, FieldValidationException):
                field = e.field
                getattr(self, field).errors += (e.mess,)
            else:
                print(e)
        return False

class EntityFormGenerator:
    '''
    Wrapper which dynamically sets the available fields on the entityForm inner
    cls. This is done to make it possible to retreive entity forms with certain 
    fields only. Only these fields are exposed to the save of the entity.
    '''

    class EntityForm(FlaskEntityForm2):
        '''
        This is just a subclass of the entityForm on which we will dynamically set
        form fields like: name = StringField() etc..
        '''
        pass

    def __init__(self, entityCls, instance=None, fields=None):
        self.entityCls = entityCls
        self.instance = instance
        if not fields:
            # no fields specified so using all fields on the entity
            fields = entityCls.fields + tuple(entityCls.typed_fields.keys())
        self.fields = fields

        for field in getattr(EntityFormGenerator.EntityForm, '_generated_fields', []):
            # clear old generated fields
            if hasattr(EntityFormGenerator.EntityForm, field):
                delattr(EntityFormGenerator.EntityForm, field)

        for field in self.fields:
            # set new fields on the form class
            setattr(EntityFormGenerator.EntityForm, field, self.get_field_type(field))
        setattr(EntityFormGenerator.EntityForm, '_generated_fields', self.fields)

    def __call__(self, **kwargs):
        '''
        Returns the actual instance of the WTForm. All kwargs are passed to the
        form together with the instance on the state (which can be None). In that
        case the EntityForm will generate new instance on save.
        '''
        return self.EntityForm(self.entityCls, instance=self.instance, **kwargs)

    def get_field_type(self, field):
        '''
        return the proper field type for the corresponding field on the entity
        '''
        if field in self.entityCls.fields:
            return StringField(field)
        if field in self.entityCls.typed_fields.keys():
            typed_field = self.entityCls.typed_fields[field]
            if typed_field.pytype == str:
                return StringField(field)
            if typed_field.pytype == list:
                return FieldList(StringField(field), min_entries=2)
            if isinstance(typed_field, EnumField):
                return SelectField(
                    field,
                    choices=tuple((x.value, x.name) for x in typed_field.enum_type))
            else:
                return StringField(field)
