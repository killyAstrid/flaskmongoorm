import os
from datetime import datetime

from bson.objectid import ObjectId
from bson.errors import InvalidId
from bson.py3compat import abc
from pymongo.cursor import Cursor
from pymongo import ASCENDING, DESCENDING
from pydantic import BaseModel

from flask import current_app as app

from .exceptions import FieldValidationException, ValidationException


class Entity:
    '''
    Base class for all mongodb persisted entities.
    '''

    # These fields are set on all entities and are not to be touched outside this class.
    __base_fields__ = (
        'created',
        'updated',
        'active',
    )

    # Collection to which mongo will associate the entities
    collection_name = None

    # All normal fields which can be set on the child class extending this entity
    fields = tuple()

    # These fields are checked for None values
    # TODO: check strings for empty strings
    required_fields = tuple()

    # Fields which require special attention when converting from and to python types
    # from and to mongo/json types
    typed_fields = {}

    indexes = (
        {'fields': [("created", DESCENDING)]},
        {'fields': [("updated", DESCENDING)]},
        {'fields': [("active", DESCENDING)]}
    )

    def __init__(self, data={}, **kwargs):
        '''
        Set the fields passed to constructor as attributes
        '''

        for field in self.fields + self.__base_fields__:
            setattr(self, field, None)
        # default field values (default defaults to None ;) )
        for key, value in self.typed_fields.items():
            setattr(self, key, value.default)
        self._id = None
        data.update(kwargs)
        self.set_fields(data)

    def validate(self, **kwargs):
        '''
        Check if fields are not none when saving, extra fields which can be passed to save
        will be taken into account via **kwargs
        '''
        for key in self.required_fields:
            value = kwargs.get(key, None) or getattr(self, key, None)
            self.validate_empty(key, value, **kwargs)
            self.validate_type(key, value, **kwargs)
        return True

    def validate_type(self, field, value, **kwargs):
        if not field in self.typed_fields:
            return True
        if not self.typed_fields[field].validate_type(value):
            raise FieldValidationException("Value {} for {} should be of type {}, not {}".
                                           format(value, field, self.typed_fields[field].pytype, type(value)), field)
        return True

    def validate_empty(self, field, value, **kwargs):
        if value == None:
            raise FieldValidationException(
                "Field {} cannot be unset or empty".format(field), field)
        if field in self.typed_fields:
            if self.typed_fields[field].is_empty_or_none(value):
                raise FieldValidationException(
                    "Field {} cannot be unset or empty".format(field), field)
        return True

    def set_active(self, value=False):
        app.mongo.db[self.collection_name].update_one(
            {'_id': self._id}, {'$set': {'active': value}})

    def set_fields(self, data):
        '''
        Parses all the data to the right python type
        '''
        for key, value in data.items():
            if value == '':
                # empty strings may remain None
                continue
            # set the id
            if key == '_id':
                try:
                    id = ObjectId(value)
                    setattr(self, key, id)
                except InvalidId:
                    pass  # self_id remains None
            # set typed fields
            if key in self.typed_fields.keys():
                if not type(value) == type(self.typed_fields[key]):
                    # this can raise exception, set to none?
                    setattr(self, key, self.typed_fields[key].to_python(value))
                else:
                    setattr(self, key, value)
            else:
                setattr(self, key, value)

    def save(self, **kwargs):
        '''
        Save instance to mongo, pass extra field values via kwargs
        '''
        self.set_fields(kwargs)
        self.validate()
        data_attributes = {k: getattr(self, k) for k in (
            self.fields + tuple(self.typed_fields.keys()))}
        # overwrite any fields which are present in kwargs
        # data_attributes.update(kwargs)

        Entity.clean_fields(data_attributes)

        for typed_field in self.typed_fields:
            # update typed fields
            if typed_field in data_attributes.keys():
                data_attributes[typed_field] = self.typed_fields[typed_field].to_persistance(
                    data_attributes[typed_field])
        if not self._id:
            data_attributes.update(
                {'created': datetime.now(), 'updated': datetime.now(), 'active': True})
            result = app.mongo.db[self.collection_name].insert_one(
                data_attributes)
            self._id = str(result.inserted_id)
            return result
        data_attributes.update({'updated': datetime.now()})
        return app.mongo.db[self.collection_name].update_one({'_id': self._id}, {'$set': data_attributes})

    def delete(self):
        return app.mongo.db[self.collection_name].delete_one({'_id': self._id})

    @classmethod
    def clean_fields(cls, fields):
        '''
        If base fields are passed to save we remove them here
        '''
        for key in cls.__base_fields__:
            fields.pop(key, None)

    @classmethod
    def new(cls, *args, **kwargs):
        '''
        method which can generate new isntances of the child class
        '''
        return cls(*args, **kwargs)

    @classmethod
    def find_one(cls, filter=None, *args, **kwargs):

        if (filter is not None and not
                isinstance(filter, abc.Mapping)):
            filter = {"_id": filter}
        cursor = cls.find(filter, *args, **kwargs)
        for result in cursor.limit(-1):
            return result
        return None

    @classmethod
    def find(cls, *args, **kwargs):
        '''
        Wrap mongo cursor to get entities when iterating over cursor
        '''
        return EntityIterator(cls, *args, **kwargs)

    def json_format(self):
        data = {k: getattr(self, k)
                for k in (self.fields + self.__base_fields__)}
        typed_fields_data = {k: v.to_persistance(
            getattr(self, k)) for k, v in self.typed_fields.items()}
        data.update({"_id": str(self._id)})
        data.update(typed_fields_data)
        return data


class HasFile:

    def get_file_type(self, x): return None

    def __init__(self, data={}, **kwargs):
        file = kwargs.get('file') or data.get('file')
        if file:
            setattr(self, 'file', file)
        super().__init__(data=data, **kwargs)

    def get_file_path(self, ff):
        return str(os.path.join(app.config['UPLOAD_DIR'], ff + '_' + str(self._id)))

    def validate_file(self, file):
        try:
            # check for magic bytes here
            self.file_type = self.get_file_type(file.filename.split('.')[-1])
        except KeyError:
            raise ValidationException("NOT VALID FILE FORMAT")

    def save(self, **kwargs):
        file = kwargs.get('file') or getattr(self, 'file', None)
        if file:
            self.validate_file(file)
        result = super().save(**kwargs)
        if file:
            file.save(self.get_file_path('file'))
        return result

    def delete(self):
        try:
            os.remove(self.get_file_path('file'))
        except FileNotFoundError:
            pass
        return super().delete()


class EntityBaseModel(BaseModel):

    class Config:
        use_enum_values = True
        validate_assignment = True


class TimestampedMainModel(EntityBaseModel):

    created: datetime = None
    updated: datetime = None
    active: bool = True
    id: str = None


class PydanticAttributeMixin:
    """
    Attributes that are on the Pydantic BaseModel of the entity are set
    and get from the BaseModel. This is done to use the validation functionality
    provided by the pydantic basemodel.
    """

    def __setattr__(self, name, value):
        if name == '_id':
            self._model.__setattr__('id', value)
        try:
            if name in self._model.__dict__:
                self._model.__setattr__(name, value)
                return
        except AttributeError:
            pass
        super().__setattr__(name, value)

    def __getattribute__(self, name):
        try:
            if name in object.__getattribute__(self, '_model').__fields__:
                return self._model.__getattribute__(name)
            if name == '_id':
                return self._model.__getattribute__('id')
        except AttributeError:
            pass
        return super().__getattribute__(name)


class PydanticEntity(PydanticAttributeMixin):

    # Collection to which mongo will associate the entities
    collection_name = None

    indexes = (
        {'fields': [("created", DESCENDING)]},
        {'fields': [("updated", DESCENDING)]},
        {'fields': [("active", DESCENDING)]}
    )

    _model_class = None

    def __init__(self, data={}, **kwargs):
        '''
        Set the fields passed to constructor as attributes
        '''
        data.update({'id': str(data.get('_id'))})
        data.update(kwargs)
        self._model = self._model_class(**data)

    def set_active(self, value=False):
        app.mongo.db[self.collection_name].update_one(
            {'_id': ObjectId(self.id)}, {'$set': {'active': value}})

    def save(self, **kwargs):
        '''
        Save instance to mongo, pass extra field values via kwargs
        '''
        for k, v in kwargs.items():
            # update model with kwargs to validate/convert values
            setattr(self, k, v)
        data = self._model.dict()
        # bug that None is converted to str(None) by pydantic?
        if self._id is None or self._id == 'None':
            data.update(
                {'created': datetime.now(), 'updated': datetime.now()})
            result = app.mongo.db[self.collection_name].insert_one(
                data)
            self._id = str(result.inserted_id)
            return result
        data.update({'updated': datetime.now()})
        return app.mongo.db[self.collection_name].update_one({'_id': ObjectId(self.id)}, {'$set': data})

    def delete(self):
        return app.mongo.db[self.collection_name].delete_one({'_id': ObjectId(self.id)})

    @classmethod
    def new(cls, *args, **kwargs):
        '''
        method which can generate new isntances of the child class
        '''
        return cls(*args, **kwargs)

    @classmethod
    def find_one(cls, filter=None, *args, **kwargs):
        if (filter is not None and (isinstance(filter, str) or isinstance(filter, ObjectId))):
            filter = {"_id": ObjectId(filter)}
        cursor = cls.find(filter, *args, **kwargs)
        for result in cursor.limit(-1):
            return result
        return None

    @classmethod
    def find(cls, *args, **kwargs):
        '''
        Wrap mongo cursor to get entities when iterating over cursor
        '''
        return EntityIterator(cls, *args, **kwargs)

    def json_format(self):
        return self._model.dict()

    def schema(self):
        return self._model.schema()


class EntityIterator(Cursor):

    def __init__(self, entity_class, *args, **kwargs):
        super().__init__(
            app.mongo.db[entity_class.collection_name], *args, **kwargs)
        self.entity_class = entity_class

    def __next__(self):
        return self.entity_class.new(super().next())

    next = __next__

    def __iter__(self):
        return self
