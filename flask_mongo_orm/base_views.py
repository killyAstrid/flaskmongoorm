from flask import current_app as app, abort, request, redirect, url_for, render_template
from flask.views import MethodView
from flask_login import current_user
from flask_wtf import FlaskForm
from pymongo.results import UpdateResult
from pydantic.error_wrappers import ValidationError
from flask_jwt import _jwt_required, current_identity, JWTError

from .forms import EntityFormGenerator


class EntityViewBase(MethodView):
    '''
    Dispatches request:
    First this class will find the entity view provider for this entity. This can be a rest
    or template provider. Afterward it will check permissions and disp the request to flask
    method view.
    '''

    api = False

    def dispatch_request(self, *args, **kwargs):
        collection_name = kwargs.get('collection_name')
        entity_id = kwargs.get('_id')
        if not collection_name:
            abort(404)
        if self.api:
            self.view_provider = app.entity_registry.rest_view_entities.get(
                collection_name)
        else:
            self.view_provider = app.entity_registry.template_view_entities.get(
                collection_name)
        if not self.view_provider:
            abort(404)
        if request.method != 'HEAD':
            if self.api:
                try:
                    _jwt_required('Login Required')
                except JWTError as e:
                    if e.error != 'Authorization Required':
                        raise e
                self.view_provider.validate_user_method_access_roles(
                    current_identity, request.method, entity_id)
            else:
                self.view_provider.validate_user_method_access_roles(
                    current_user, request.method, entity_id)
        return super().dispatch_request(*args, **kwargs)


class EntityRestView(EntityViewBase):

    api = True
    # decorators = (jwt_required,)

    def get(self, collection_name, _id):
        if not _id:
            # provide list of entities
            return {'entries': [ent.json_format() for ent in self.view_provider.get_objects()]}
        else:
            return self.view_provider.get_object(_id=_id).json_format()

    def post(self, collection_name, _id):
        try:
            result = self.view_provider.create_update(request.json, _id=_id)
            if isinstance(result, FlaskForm):
                return {'errors': result.errors}, 400
            if isinstance(result, UpdateResult):
                return {'upserted': result.acknowledged}
            return {'inserted_id': str(result.inserted_id)}, 201
        except ValidationError as e:
            return {'errors': str(e)}

    def delete(self, collection_name, _id):
        result = self.view_provider.delete(_id=_id)
        return {'deleted': result.acknowledged}


class EntityTMPLTView(EntityViewBase):

    def _detail_view(self, _id=None, form=None):
        if not form:
            # no form provided, so create a new one with entity on the form
            entity = self.view_provider.get_object(_id)
            form = EntityFormGenerator(
                self.view_provider.entityCls, instance=entity)()
        return render_template(
            'entity_details.html',
            form=form,
            title='{} detail view'.format(
                self.view_provider.entityCls.collection_name),
            header=self.view_provider.entityCls.collection_name,
            collection=self.view_provider.entityCls.collection_name,
            url_name_prefix=self.view_provider.url_name_prefix
        )

    def get(self, collection_name, _id, form=None):
        if not _id:
            entities = [ent.json_format()
                        for ent in self.view_provider.get_objects()]
            return render_template(
                'entity_list.html',
                entities=entities,
                title='{} list view'.format(
                    self.view_provider.entityCls.collection_name),
                header=self.view_provider.entityCls.collection_name,
                collection=self.view_provider.entityCls.collection_name,
            )
        elif _id == 'create':
            return self._detail_view()
        else:
            return self._detail_view(_id=_id)

    def post(self, collection_name, _id):
        if _id == 'create':
            _id = None
        result = self.view_provider.create_update(dict(request.form), _id=_id)
        if isinstance(result, FlaskForm):
            # error during saving, hence return the form with errors
            return self._detail_view(form=result)
        return redirect(
            url_for(
                '{}.entity'.format(
                    self.view_provider.url_name_prefix),
                collection_name=self.view_provider.entityCls.collection_name
            )
        )

    def delete(self, collection_name, _id):
        self.view_provider.delete(_id=_id)
        return redirect(
            url_for('{}.entity'.format(
                self.view_provider.url_name_prefix,
                collection_name=self.view_provider.entityCls.collection_name)
            )
        )
