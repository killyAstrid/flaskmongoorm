
class ValidationException(Exception):
    pass

class FieldValidationException(ValidationException):

    def __init__(self, mess, field):
        self.mess = mess
        self.field = field

    def __str__(self):
        return self.mess