from enum import Enum

class Field:
    '''
    Field offers some help functions used in validation and converting between typed
    fields on the entity. It can help keeping the persistent data consistent. Mongo
    does not constrain datatypes of fields out of the box, so the value of 'some_field'
    can be equal to [1,2,3] on one instance, whilst it can be {'count': 1000} on another.
    If pytype is set on this class, the validate will enforce python type before the save 
    of the data. If you have complexer python objects which need to be persisted, you 
    can provide a conversion function.
    '''

    def __init__(self, to_perisistance_type=lambda x: x, to_python_type=lambda x: x, pytype=None, default=None):
        self.to_perisistance_type = to_perisistance_type
        self.to_python_type = to_python_type
        self.pytype = pytype
        self.default = default
    
    def to_python(self, persistance_value):
        if persistance_value == None:
            return None
        return self.to_python_type(persistance_value)

    def to_persistance(self, python_value):
        if python_value == None:
            return None
        if self.pytype == list:
            # if we have empty values in list remove
            python_value = [x for x in python_value if x and type(x) is not (bool or int)]
        return self.to_perisistance_type(python_value)

    def validate_type(self, python_value):
        # validate python value which will be persisted in db
        # None is always a valid type
        if self.pytype and not python_value == None:
            return isinstance(python_value, self.pytype)
        return True

    def is_empty_or_none(self, python_value):
        # evaluate if the field has an empty or None value
        if python_value == None:
            return True
        if self.pytype == str and not python_value:
            return True
        return False

class EnumField(Field):

    def __init__(self, enum_type):
        self.enum_type = enum_type
        super().__init__(
            to_perisistance_type=lambda x: x.value if x else None,
            to_python_type=lambda x: enum_type(int(x))
        )
    
    def __iter__(self):
        return iter(tuple(self.enum_type))