from flask import abort, Blueprint, render_template, redirect, url_for
from bson.objectid import ObjectId

from .forms import EntityFormGenerator
from .base_views import EntityRestView, EntityTMPLTView


class EntityViewLogicProvider:
    '''
    This class is created for each entity for the template as well as the rest
    views. The class provides some basic interactions with the underlying collection
    aswell as some logic to validate operations.
    '''

    def __init__(
        self,
        entityCls,
        url_name_prefix,
        methods={'C': tuple(), 'R': tuple(), 'U': tuple(), 'D': tuple()},
    ):
        self.entityCls = entityCls
        self.methods = methods
        self.url_name_prefix = url_name_prefix

    def validate_user_method_access_roles(self, current_user, method, _id):
        action = None
        if method.lower() == 'get':
            action = 'R'
        elif method.lower() == 'post' and _id == None:
            action = 'C'
        elif method.lower() == 'post':
            action = 'U'
        elif method.lower() == 'delete':
            action = 'D'
        if not action:
            abort(405)
        if not self.methods[action]:
            return True
        for allowed_role in self.methods[action]:
            if allowed_role in getattr(current_user, 'roles', []):
                return True
        abort(401)

    def get_object(self, _id):
        if not _id:
            return None
        entity = self.entityCls.find_one(ObjectId(_id))
        if not entity:
            abort(404)
        return entity

    def get_objects(self, filter=None):
        return self.entityCls.find(filter=filter)

    def create_update(self, data, _id=None):
        data['_id'] = ObjectId(_id)
        instance = self.get_object(_id)
        form = EntityFormGenerator(
            self.entityCls, instance=instance)(data=data)
        return form.save() or form

    def delete(self, _id):
        return self.get_object(_id).delete()


class ApiEntityViewLogicProvider(EntityViewLogicProvider):

    def create_update(self, data, _id=None):
        data['_id'] = None if _id == None else str(_id)
        instance = self.get_object(_id)
        if not instance:
            instance = self.entityCls.new(**data)
            return instance.save()
        return instance.save(**data)


class EntityRegistry:
    '''
    One instance is created for a Flask app. This instance contains information about the 
    entity collections which are registered for the template and rest views. It can be
    configured to have diffentent url names and url prefixes.
    '''

    def __init__(self, app=None, mongo=None, url_name_prefix='entity_template_views',
                 rest_url_name_prefix='entity-api', rest_view_url_prefix='api',
                 template_view_url_prefix='entities'):
        self.template_view_entities = {}
        self.rest_view_entities = {}

        self.url_name_prefix = url_name_prefix
        self.rest_url_name_prefix = rest_url_name_prefix

        self.template_view_url_prefix = template_view_url_prefix
        self.rest_view_url_prefix = rest_view_url_prefix

        self.entities = {}

        if app and mongo:
            self.init_app(app, mongo)

    def init_app(self, app, mongo):
        app.mongo = mongo
        app.entity_registry = self
        self.register_endpoints(app)
        self.bootstrap_indexes(app, mongo)

    def register(self, **options):
        """A decorator that is used to register an entity for a view.
        :param options::
            Forwarded to EntityTemplateViewProvider
            - methods {'C': ('ADMIN',), 'R':tuple(), 'U':tuple(), 'D':tuple()}
        """
        def decorator(cls):
            self.template_view_entities[cls.collection_name] = EntityViewLogicProvider(
                cls, self.url_name_prefix, **options)
            return cls
        return decorator

    def restgister(self, **options):
        """A decorator that is used to register an entity for a rest view.
        :param options::
            Forwarded to EntityRestViewProvider
            - methods {'C': ('ADMIN',), 'R':tuple(), 'U':tuple(), 'D':tuple()}
        """
        def decorator(cls):
            self.rest_view_entities[cls.collection_name] = ApiEntityViewLogicProvider(
                cls, self.rest_url_name_prefix, **options)
            return cls
        return decorator

    def register_collection_bootstrap(self):

        def decorator(cls):
            self.collections = {}
            self.entities[cls.collection_name] = cls
            return cls
        return decorator

    def bootstrap_indexes(self, app, mongo):
        for k, v in self.entities.items():
            app.logger.info(f'Bootstrapping entity collection: {k}..')

            old_indexes = mongo.db[k].index_information()
            old_indexes.pop('_id_', None)
            old_indexes_keys = old_indexes.keys()
            current_indexes = []

            for index in v.indexes:
                result = mongo.db[k].create_index(
                    index['fields'], **index.get('options', {}))
                current_indexes.append(result)
                app.logger.info(f'Done creating index {result}')

            for old_index in old_indexes_keys:
                if old_index not in current_indexes:
                    app.logger.info(f'Dropping old index {old_index}')
                    mongo.db[k].drop_index(old_index)
            app.logger.info('New updated indexes {}'.format(
                mongo.db[k].index_information()))

    def register_endpoints(self, app):
        '''
        Make nicer
        Method that registers api and template endpoints
        '''

        app.logger.info('Registering entity blueprint under blueprint name {} with url prefix /{}'
                        .format(self.url_name_prefix, self.template_view_url_prefix))
        entity_template_blueprint = Blueprint(self.url_name_prefix, __name__)

        entity_template_blueprint.add_url_rule('/{}/<collection_name>/'
                                               .format(self.template_view_url_prefix),
                                               defaults={'_id': None},
                                               view_func=EntityTMPLTView.as_view(
                                                   'entity',),
                                               methods=['GET', 'POST']
                                               )

        entity_template_blueprint.add_url_rule('/{}/<collection_name>/<_id>/'
                                               .format(self.template_view_url_prefix),
                                               view_func=EntityTMPLTView.as_view(
                                                   'entity-detail',),
                                               methods=[
                                                   'GET', 'POST', 'DELETE']
                                               )
        app.register_blueprint(entity_template_blueprint)
        app.logger.info('Done registering entity endoints')

        entity_rest_blueprint = Blueprint(self.rest_url_name_prefix, __name__)
        app.logger.info('Registering entity rest blueprint under blueprint name {} with url prefix /{}'
                        .format(self.rest_url_name_prefix, self.rest_view_url_prefix))

        entity_rest_blueprint.add_url_rule('/{}/<collection_name>/'
                                           .format(self.rest_view_url_prefix),
                                           defaults={'_id': None},
                                           view_func=EntityRestView.as_view(
                                               'entity',),
                                           methods=['GET', 'POST']
                                           )

        entity_rest_blueprint.add_url_rule('/{}/<collection_name>/<_id>/'
                                           .format(self.rest_view_url_prefix),
                                           view_func=EntityRestView.as_view(
                                               'entity-detail',),
                                           methods=['GET', 'POST', 'DELETE']
                                           )
        app.register_blueprint(entity_rest_blueprint)
        app.logger.info('Done registering entity rest endoints')
