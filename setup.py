from setuptools import setup

setup(
    name='Flask-Mongo-ORM',
    version='1.0',
    author='Abel Stam',
    author_email='abelstam@hotmail.com',
    packages=['flask_mongo_orm'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Flask',
        'Flask-PyMongo',
        'flask_wtf',
        'flask-login',
        'Flask-JWT>=0.3.2',
        'pydantic>=1.4'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
